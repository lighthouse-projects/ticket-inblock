const solc = require('solc');
const fs = require('fs-extra');
const path = require('path');

const buildPath = path.resolve(__dirname, 'build');
const contractsPath = path.resolve(__dirname, 'contracts');

compile = (contracts) => {
    console.log('Compiling smart contracts...')
    const output = solc.compile({sources: contracts}, 1, findImports);
    if (output.errors) {
        console.error(output.errors);
    }
    for (let contract in output.contracts) {
        console.log(contract, 'compiled');
        const target = path.resolve(buildPath, contract.split(':')[0].split('.')[0] + ".json");
        fs.writeJsonSync(target, output.contracts[contract]);
    }
}

findImports = (import_path) => {
    const sourcePath = path.resolve(contractsPath, import_path);
    const source = fs.readFileSync(sourcePath, 'utf-8');

    return {contents: source};
}

compileContract = (path) => {
    const source = fs.readFileSync(path, 'utf-8');
    const file = path.substring(contractsPath.length + 1);

    compile({[file]: source});
}

execute = () => {
    // 1. Recreate build folder
    fs.removeSync(buildPath);
    fs.ensureDirSync(buildPath);
    fs.ensureDirSync(path.resolve(buildPath, 'interfaces'));

    // 2. Compile contracts
    let contracts = {};
    fs.readdirSync(contractsPath).forEach(file => {
        if (file.includes('.sol')) {
            const sourcePath = path.resolve(contractsPath, file);
            if (!fs.lstatSync(sourcePath).isDirectory()) {
                const source = fs.readFileSync(sourcePath, 'utf-8');
                contracts[file] = source;
            }
        }
    });

    compile(contracts);
}

module.exports = execute;