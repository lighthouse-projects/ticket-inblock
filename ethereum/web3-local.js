const Web3 = require('web3');
const ganache = require('ganache-cli');

module.exports = new Web3(ganache.provider());