pragma solidity ^0.4.24;

contract MatchToken {
    
    // Local team
    string public local_team;
    // Visitor team
    string public visitor_team;
    // Date of the match
    uint256 public match_date;
    // Stadium
    string public stadium;
    // Total supply
    uint256 public totalSupply;
    // Sponsor of the match
    address public sponsor;

    // Balance by account
    mapping(address => uint256) public balances;
    // Allowances for transferFrom
    mapping(address => mapping(address => uint256)) public allowances;

    // EVENTS
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

    constructor (
        string _local_team,
        string _visitor_team,
        uint256 _match_date,
        string _stadium,
        uint256 _totalSupply
    ) public {
        require(bytes(_local_team).length > 0, "Local team empty");
        require(bytes(_visitor_team).length > 0, "Visitor team empty");
        require(_match_date > 0, "The date of the match is empty");
        require(_match_date > now, "Match created in the past");
        require(bytes(_stadium).length > 0, "Stadium is empty");
        require(_totalSupply > 0, "Number of seats empty or 0");
        local_team = _local_team;
        visitor_team = _visitor_team;
        match_date = _match_date;
        stadium = _stadium;
        totalSupply = _totalSupply;
        sponsor = msg.sender;
        balances[sponsor] = totalSupply;

        emit Transfer(address(0), msg.sender, totalSupply);
    }

    // Returns the account balance of another account with address _owner.
    function balanceOf(address _owner) public view returns (uint256) {
        return balances[_owner];
    }

    // Transfers _value amount of tokens to address _to, and MUST fire the Transfer event.
    // The function SHOULD throw if the _from account balance does not have enough tokens to spend.
    //
    // Note Transfers of 0 values MUST be treated as normal transfers and fire the Transfer event.
    function transfer(address _to, uint256 _value) public returns (bool) {
        require(balances[msg.sender] >= _value, "There is not enough balance for the transfer");
        balances[msg.sender] -= _value;
        balances[_to] += _value;
 
        emit Transfer(msg.sender, _to, _value);
        return true;
    }

    // Transfers _value amount of tokens from address _from to address _to, and MUST fire the Transfer event.
    //
    // The transferFrom method is used for a withdraw workflow, allowing contracts to transfer tokens on your behalf.
    // This can be used for example to allow a contract to transfer tokens on your behalf and/or to charge fees in sub-currencies.
    // The function SHOULD throw unless the _from account has deliberately authorized the sender of the message via some mechanism.
    //
    // Note Transfers of 0 values MUST be treated as normal transfers and fire the Transfer event.
    function transferFrom(address _from, address _to, uint256 _value) public returns (bool) {
        require (balances[_from] >= _value, "Not enough balance");
        require (allowances[_from][msg.sender] >= _value, "Not enough allowed balance");

        allowances[_from][msg.sender] -= _value;
        balances[_from] -= _value;
        balances[_to] += _value;

        emit Transfer(_from, _to, _value);
        return true;
    }

    // Allows _spender to withdraw from your account multiple times, up to the _value amount.
    // If this function is called again it overwrites the current allowance with _value.
    function approve(address _spender, uint256 _value) public returns (bool) {
        allowances[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    // Returns the amount which _spender is still allowed to withdraw from _owner.
    function allowance(address _owner, address _spender) public view returns (uint256) {
        return (allowances[_owner][_spender]);
    }

}