pragma solidity ^0.4.24;

import "./MatchToken.sol";

contract MatchTokenMarketplace {
    struct MatchTokenOnSale {
        address seller;
        MatchToken token;
        uint256 unitsAvailable;

        // wei/unit price as a rational number
        uint256 priceNumerator;
        uint256 priceDenominator;
    }

    string public name;
    address public owner;

    MatchTokenOnSale[] public matchMarketplace;

    // EVENTS
    event MatchMarketplaceChanged(address indexed _seller, uint256 indexed _index, uint256 _units);

    constructor (string _name) public {
        name = _name;
        owner = msg.sender;
    }
    
    function sell(
        MatchToken token,
        uint256 units,
        uint256 numerator,
        uint256 denominator
    ) public {
        MatchTokenOnSale memory matchTokenOnSale = MatchTokenOnSale({
            seller: msg.sender,
            token: token,
            unitsAvailable: units,
            priceNumerator: numerator,
            priceDenominator: denominator
        });

        matchMarketplace.push(matchTokenOnSale);

        emit MatchMarketplaceChanged(msg.sender, matchMarketplace.length-1, units);
    }

    function cancel(uint256 index, uint256 units) public {
        require(matchMarketplace[index].seller == msg.sender, "Just the seller of the token can cancel the market of the token");
        require((index >= 0 && index < matchMarketplace.length), "Row to remove does not exist");
        if (matchMarketplace[index].unitsAvailable > units) {
            matchMarketplace[index].unitsAvailable -= units;
            emit MatchMarketplaceChanged(msg.sender, index, matchMarketplace[index].unitsAvailable);
        } else {
            if (matchMarketplace.length == 1) {
                delete matchMarketplace;
            } else {
                matchMarketplace[index] = matchMarketplace[matchMarketplace.length - 1];
                matchMarketplace.length--;
            }
            emit MatchMarketplaceChanged(msg.sender, index, 0);
        }
    }

    function buy(uint256 index, uint256 units) public payable {
        MatchTokenOnSale storage matchTokenOnSale = matchMarketplace[index];
        require(matchTokenOnSale.unitsAvailable >= units, "The number of units to buy is bigger than available units");

        matchTokenOnSale.unitsAvailable -= units;
        matchTokenOnSale.token.transferFrom(matchTokenOnSale.seller, msg.sender, units);
        uint256 cost = (units * matchTokenOnSale.priceNumerator) /
            matchTokenOnSale.priceDenominator;
        require(msg.value == cost, "The value of the transaction is different of the cost of the transfer");
        matchTokenOnSale.seller.transfer(cost);

        emit MatchMarketplaceChanged(matchTokenOnSale.seller, index, matchTokenOnSale.unitsAvailable);
    }

    function getMatchMarketplaceLength() public view returns (uint256) {
        return matchMarketplace.length;
    }

}