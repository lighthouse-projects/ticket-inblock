https://app.mindmup.com/map/_free/2018/10/8609bb00d5bd11e8af2f4739bf698cff
** CUCUMBER **

https://docs.cucumber.io/guides/10-minute-tutorial/

1. Cucumber installation in project

$ npm install cucumber --save-dev

In package.json:

  "scripts": {
    "test": "cucumber-js"
  }

$ mkdir features
$ mkdir features/step_definitions
Create a file called cucumber.js at the root of your project and add the following content:

module.exports = {
  default: `--format-options '{"snippetInterface": "synchronous"}'`
}

Also, create a file called features/step_definitions/stepdefs.js with the following content:

const assert = require('assert');
const { Given, When, Then } = require('cucumber');

** "Cucumber (Gherkin) Full Supoort" EXTENSION FOR VS CODE **
In project directory:
$ mkdir .vscode && touch .vscode/settings.json
{
    "cucumberautocomplete.steps": [
        "test/features/step_definitions/*.js",
        "node_modules/qa-lib/src/step_definitions/*.js"
    ],
    "cucumberautocomplete.syncfeatures": "test/features/*feature",
    "cucumberautocomplete.strictGherkinCompletion": true
}

** BDD **

1- Historias de usuario

2- Criterios de aceptación
Por historia, "when-then"
** SMART CONTRACT **
Invertir hashmap del balance
key: asiento (uint)
address: propietario del asiento
mapping(address => uint256) public balances;

Red privada de ethereum con criptomoneda propia que pueda emplearse para comprar entradas.
