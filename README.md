# ticket-inblock

Marketplace of tickets using smart contracts in Ethereum

### 1. Install the project (download dependencies)

npm install

### 2. Compile smart contracts

cd ethereum
node compile.js

### 3. Execute tests with cucumber

cd [ROOT_DIR]
npm run test

## Folders

**/ethereum/contracts** contains the source code of smart contracts in solidity  

**/ethereum/build** contains compiled smart contracts. The .json include the abi and the bytecode  

**/features** cucumber features written in Gherkin

## Automation of compilation and testing using gulp

### 1. Install the gulp package globally

npm install --global gulp -cli

### 2. Launch gulp from the root folder of the app

gulp