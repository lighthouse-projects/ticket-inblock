web3 = require('../ethereum/web3-local');
matchToken = require('../ethereum/build/MatchToken');

function FeaturesHelper() {
  this.date_format = 'DD-MM-YYYY';

  this.deployMatchToken = async (owner, local_team, visitor_team, match_date, stadium, capacity, callback) => {
      const moment = require('moment')(match_date, this.date_format);
      const dateUnix = moment.format('X');
      let err;
      let contract;
      try {
        const deploy = await new web3.eth.Contract(JSON.parse(matchToken.interface))
        .deploy({
          data: matchToken.bytecode,
          arguments: [local_team, visitor_team, dateUnix, stadium, capacity]
        });
        const _gas = await deploy.estimateGas();
        contract = await deploy.send({
            from: owner,
            gas: _gas 
        });
      } catch (e) {
        if (e.hashes) {
          err = e.results[e.hashes[0]];
        } else {
          err = e;
          console.error(e);
        }
        
      }
      callback(err, contract);
  }

  this.buildMatchName = (local, visitor, date) => {
    return local + "-" + visitor + " " + date;
  }

  this.deployMatchTokenMarketplace = async (owner, marketplaceName, callback) => {
    let error;
    let contract;
    try {
      const matchMarketplace = require('../ethereum/build/MatchTokenMarketplace');
      const deploy = await new web3.eth.Contract(JSON.parse(matchMarketplace.interface))
                                          .deploy({
                                              data: matchMarketplace.bytecode,
                                              arguments: [marketplaceName]
                                          });
      const _gas = await deploy.estimateGas();
      contract = await deploy.send({
          from: owner,
          gas: _gas
      });
    } catch(e) {
      error = e;
    }
    callback(error, contract);
  }

}

module.exports = new FeaturesHelper();
