# language: es
Característica: Compra de localidades en taquilla
  Compra de localidades en taquilla

  Escenario: El comprado paga las localidades compradas
    Dado 100 entradas del 'Mallorca'-'Albacete' a 28 $ 
    Y 'Siviero' tiene un saldo de 100 $
    Cuando 'Siviero' compra 2 entradas del 'Mallorca'-'Albacete'
    Entonces el saldo de 'Siviero' será 44 $ 

  Escenario: Rechazo de compra por saldo insuficiente
    Dado 'Takillator' operativo
    Y 100 entradas del 'Mallorca'-'Albacete' a 28 $ 
    Y 'Roa' tiene un saldo de 100 $
    Cuando 'Roa' compra 4 entradas del 'Mallorca'-'Albacete'
    Entonces la compra es rechazada por saldo insuficiente

  Escenario: Cobro de entradas
    Dado 125 entradas del 'Mallorca'-'Albacete' a 31 $ 
    Cuando 'Biagini' compra 2 entradas del 'Mallorca'-'Albacete'
    Entonces el saldo del organizador del 'Mallorca'-'Albacete' se incrementa en 62 $
    Y el propietario de las 2 entradas es 'Biagini'
    Y el organizador del encuentro dispone de 123

