# language: es
Característica: Venta de localidades en taquilla
  Venta de localidades en taquilla

  Escenario: El organizador del partido pone a la venta las localidades del encuentro
    Dado 'Takillator' operativo
    Y 'Beltrán' ha organizado el 'Mallorca'-'Albacete' para el '19/05/2019' en el estadio 'Mr Cat' con aforo de 23142 localidades
    Cuando 'Beltrán' pone a la venta en 'Takillator' 23142 localidades del 'Mallorca'-'Albacete' del '19/05/2019'
    Entonces En 'Takillator' hay 23142 entradas del encuentro 'Mallorca'-'Albacete' del '19/05/2019'

  Escenario: Reventa de localidades
    Dado 'Takillator' operativo
    Y 'Beltrán' ha organizado el 'Mallorca'-'Albacete' para el '19/05/2019' en el estadio 'Mr Cat' con aforo de 23142 localidades
    Cuando 'Soler' pone a la venta 3 localidades del 'Mallorca'-'Albacete' para el '19/05/2019' en 'Takillator' a 100 $ cada una
    Entonces en 'Takillator' hay 3 entradas del 'Mallorca'-'Albacete' del '19/05/2019' a 100 $ cuyo vendedor es 'Soler'

  Escenario: Retirar localidades de taquilla
    Dado 'Takillator' operativo
    Y 'Soler' tiene a la venta 3 localidades en 'Takillator' del 'Mallorca'-'Albacete' del '19/05/2019'
    Cuando 'Soler' retira de la venta 2 localidades del 'Mallorca'-'Albacete' del '19/05/2019' en 'Takillator'
    Entonces 'Soler' dispondrá de 1 localidades a la venta en 'Takillator' del 'Mallorca'-'Albacete' del '19/05/2019'