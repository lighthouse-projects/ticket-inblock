const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const web3 = require('../../ethereum/web3-local');
const featuresHelper = require('../FeaturesHelper');
let actors = {};

// State of the tests - similar to state in react -
let state = {
    actors: {},
    matches: {},
    marketplaces: {},
    error: '',
    last_sale_index: -1
};

Given('{string} operativo', async function (marketplaceName) {
    const accounts = await web3.eth.getAccounts();
    const owner = accounts[0];

    await featuresHelper.deployMatchTokenMarketplace(owner, marketplaceName, (_error, _contract) => {contract = _contract;});
    assert.ok(contract.options.address);
    state.marketplaces[marketplaceName] = contract;

    // Listen to the event MatchMarketplaceChanged to get the index of last matchtokensale updated (sell or cancel action)
    contract.events.MatchMarketplaceChanged({}, (error, event) => {state.last_sale_index = event.returnValues._index;})
    
  });

Given('{string} ha organizado el {string}-{string} para el {string} en el estadio {string} con aforo de {int} localidades', async function (sponsor, local_team, visitor_team, match_date, stadium, capacity) {
    const sponsor_address = await getOrCreateActor(sponsor);
    const match_name = featuresHelper.buildMatchName(local_team, visitor_team, match_date);
    await featuresHelper.deployMatchToken(sponsor_address, local_team, visitor_team, match_date, stadium, capacity, (_error, _contract) => {state.error = _error; state.matches[match_name] = _contract});
    assert.ok(state.matches[match_name].options.address);
  });

  When('{string} pone a la venta en {string} {int} localidades del {string}-{string} del {string}', async function (sponsor, marketplace, units, local, visitor, date) {
    const matchName = featuresHelper.buildMatchName(local, visitor, date);
    const matchContract = state.matches[matchName];
    assert.ok(matchContract);

    const marketplaceContract = state.marketplaces[marketplace];
    assert.ok(marketplaceContract);

    const method = marketplaceContract.methods.sell(matchContract.options.address, units, 0, 1);
    const _gas = await method.estimateGas();
    const sponsor_address = await getOrCreateActor(sponsor);
    assert.ok(sponsor_address);
    await method.send({
        from: sponsor_address,
        gas: _gas
    });
  });

  Then('En {string} hay {int} entradas del encuentro {string}-{string} del {string}', async function (marketplace, units, local, visitor, date) {
    const matchName = featuresHelper.buildMatchName(local, visitor, date);
    const matchContract = state.matches[matchName];
    assert.ok(matchContract);

    const marketplaceContract = state.marketplaces[marketplace];
    assert.ok(marketplaceContract);

    const count = await marketplaceContract.methods.getMatchMarketplaceLength().call();
    let numberOfSeats = 0;
    await Promise.all(
        Array(parseInt(count))
        .fill()
        .map(async (element, index) => {
            const matchToken = await marketplaceContract.methods.matchMarketplace(index).call();
            

            if (matchContract.options.address == matchToken.token) {
                numberOfSeats += matchToken.unitsAvailable;
            }
        })
    );

    assert.equal(units, numberOfSeats);
  });

  When('{string} pone a la venta {int} localidades del {string}-{string} para el {string} en {string} a {int} $ cada una', async function (seller, units, local, visitor, date, marketplace, price) {
    const seller_address = await getOrCreateActor(seller);

    const matchName = featuresHelper.buildMatchName(local, visitor, date);
    const matchContract = state.matches[matchName];
    assert.ok(matchContract);

    const marketplaceContract = state.marketplaces[marketplace];
    assert.ok(marketplaceContract);

    const method = await marketplaceContract.methods.sell(matchContract.options.address, units, price, 1);
    const _gas = await method.estimateGas();
    await method.send({
        from: seller_address,
        gas: _gas
    });
  });

  Then('en {string} hay {int} entradas del {string}-{string} del {string} a {int} $ cuyo vendedor es {string}', async function (marketplace, units, local, visitor, date, price, seller) {
    const matchName = featuresHelper.buildMatchName(local, visitor, date);
    const matchContract = state.matches[matchName];
    assert.ok(matchContract);

    const marketplaceContract = state.marketplaces[marketplace]
    assert.ok(marketplaceContract);

    const last = await marketplaceContract.methods.getMatchMarketplaceLength().call();
    const matchTokenOnSale = await marketplaceContract.methods.matchMarketplace(last-1).call();
    assert.equal(units, matchTokenOnSale.unitsAvailable);
    assert.equal(matchContract.options.address, matchTokenOnSale.token);
    assert.equal(price, matchTokenOnSale.priceNumerator); // It just check prices without decimals
    const seller_address = await getOrCreateActor(seller);
    assert.equal(seller_address, matchTokenOnSale.seller);
  });

  Given('{string} tiene a la venta {int} localidades en {string} del {string}-{string} del {string}', async function (seller, units, marketplace, local_team, visitor_team, match_date) {
    const sponsor_address = await getOrCreateActor('sponsor');
    const match_name = featuresHelper.buildMatchName(local_team, visitor_team, match_date);
    await featuresHelper.deployMatchToken(sponsor_address, local_team, visitor_team, match_date, 'stadium', 1, (_error, _contract) => {state.error = _error; state.matches[match_name] = _contract});
    assert.ok(state.matches[match_name].options.address);

    const marketplaceContract = state.marketplaces[marketplace];
    const seller_address = await getOrCreateActor(seller);
    const method = await marketplaceContract.methods.sell(state.matches[match_name].options.address, units, 0, 1);
    const _gas = await method.estimateGas();
    await method.send({
        from: seller_address,
        gas: _gas
    });
  });

  When('{string} retira de la venta {int} localidades del {string}-{string} del {string} en {string}', async function (seller, units, local_team, visitor_team, match_date, marketplace) {
    const marketplaceContract = state.marketplaces[marketplace];
    assert.ok(marketplaceContract, "The smart contract of the marketplace does not exist");

    const seller_address = await getOrCreateActor(seller);

    const method = await marketplaceContract.methods.cancel(state.last_sale_index, units);
    const _gas = await method.estimateGas();
    await method.send({
        from: seller_address,
        gas: _gas
    });
  });

  Then('{string} dispondrá de {int} localidades a la venta en {string} del {string}-{string} del {string}', async function (seller, units, marketplace, local_team, visitor_team, match_date) {
    const marketplaceContract = state.marketplaces[marketplace];
    assert.ok(marketplaceContract, "The smart contract of the match does not exist");

    const matchToken = await marketplaceContract.methods.matchMarketplace(state.last_sale_index).call();
    assert.equal(units, matchToken.unitsAvailable);
  });

  getOrCreateActor = async (name) => {
    // Get the account of the sponsor
    if (!state.actors[name]) {
        const accounts = await web3.eth.getAccounts();
        state.actors[name] = accounts[Object.keys(actors).length]
    }
    return state.actors[name];
  }
  