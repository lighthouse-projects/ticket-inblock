const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const web3 = require('../../ethereum/web3-local');
const MatchToken = require('../../ethereum/build/MatchToken');
const featuresHelper = require('../FeaturesHelper');

const gasPrice = web3.utils.toWei('1', 'gwei');

let state = {
    marketplace: '',
    actors: {},
    error: '',
    last_sale_index: -1,
    gasUsedInLastBuy: 0
}

Given('{int} entradas del {string}-{string} a {int} $', async function (units, local_team, visitor_team, price) {
  const sponsor_address = await getOrCreateBuyActor('sponsor');
  
  // Create match
  let match;
  let match_date = new Date();
  match_date.setDate(match_date.getDate() + 10); // add 10 days
  const moment = require('moment')(match_date);
  match_date = moment.format(featuresHelper.date_format);

  await featuresHelper.deployMatchToken(sponsor_address, local_team, visitor_team, match_date, 'stadium', units, (_error, _contract) => {match = _contract});
  assert.ok(match.options.address, 'Match contract not deployed!');

  // Create marketplace
  let marketplace;
  await featuresHelper.deployMatchTokenMarketplace(sponsor_address, 'Takillator', (_error, _contract) => {marketplace = _contract;});
  assert.ok(marketplace.options.address);
  marketplace.events.MatchMarketplaceChanged({}, (error, event) => {state.last_sale_index = event.returnValues._index;})
  state.marketplace = marketplace;

  // Sell tickets in marketplace
  let method = marketplace.methods.sell(match.options.address, units, web3.utils.toWei(String(price), 'ether'), 1);
  let _gas = await method.estimateGas();
  await method.send({
      from: sponsor_address,
      gas: _gas
  });
  method = match.methods.approve(marketplace.options.address, units);
  _gas = await method.estimateGas();
  await method.send({
      from: sponsor_address,
      gas: _gas
  })

});

Given('{string} tiene un saldo de {int} $', async function (buyer, balance) {
  const buyer_address = await getOrCreateBuyActor(buyer);
  const b = await web3.eth.getBalance(buyer_address);
  assert.equal(balance, web3.utils.fromWei(b, 'ether'), 'Please set the balance of the buyer to default balance of ganache-cli');
});

When('{string} compra {int} entradas del {string}-{string}', async function (buyer, units, local, visitor) {
  assert.ok(state.last_sale_index, 'Please put seats on sale');
  const buyer_address = await getOrCreateBuyActor(buyer);
  // Get price of the match
  const matchToken = await state.marketplace.methods.matchMarketplace(state.last_sale_index).call();

  // Save balance of the sponsor of the match (used in future steps)
  const match = await new web3.eth.Contract(JSON.parse(MatchToken.interface), matchToken.token);
  const sponsor = await match.methods.sponsor().call();
  state.sponsor_balance = await web3.eth.getBalance(sponsor);
  
  // Buy the seats
  let amount = units * matchToken.priceNumerator;
  const method = state.marketplace.methods.buy(state.last_sale_index, units);

  const _gas = await method.estimateGas({
    from: buyer_address,
    value: amount
  });

  try {
    const hash = await method.send({
        from: buyer_address,
        gas: _gas,
        gasPrice: gasPrice,
        value: amount
    });
    state.gasUsedInLastBuy = hash.gasUsed;
  } catch(e) {
    state.error = e;
  }
});

Then('el saldo de {string} será {int} $', async function (buyer, new_balance) {
    const buyer_address = await getOrCreateBuyActor(buyer);
    const balance = await web3.eth.getBalance(buyer_address);
    // Add transaction fee to the balance because the test is based on the amount of the transaction without fees
    const netBalance = (parseInt(balance) + state.gasUsedInLastBuy*gasPrice);
    assert.equal(web3.utils.toWei(String(new_balance), 'ether'), netBalance);
});

Then('la compra es rechazada por saldo insuficiente', function () {
  // sender doesn't have enough funds to send tx

  assert.ok(state.error);
  assert.ok(state.error.toString().indexOf('sender doesn\'t have enough funds to send tx')>=0);
});

Then('el saldo del organizador del {string}-{string} se incrementa en {int} $', async function (string, string2, amount) {
  const address = await getOrCreateBuyActor('sponsor')
  const currentBalance = await web3.eth.getBalance(address);
  const balanceExpected = parseInt(state.sponsor_balance) + parseInt(web3.utils.toWei(String(amount), 'ether'));
  assert.equal(balanceExpected, currentBalance);
});

Then('el propietario de las {int} entradas es {string}', async function (units, buyer) {
  const matchToken = await state.marketplace.methods.matchMarketplace(state.last_sale_index).call();
  const match = await new web3.eth.Contract(JSON.parse(MatchToken.interface), matchToken.token);
  const buyer_address = await getOrCreateBuyActor(buyer);
  const seats = await match.methods.balanceOf(buyer_address).call();

  assert.equal(units, seats);
});

Then('el organizador del encuentro dispone de {int}', async function (units) {
  const matchToken = await state.marketplace.methods.matchMarketplace(state.last_sale_index).call();
  const match = await new web3.eth.Contract(JSON.parse(MatchToken.interface), matchToken.token);
  const sponsor_address = await getOrCreateBuyActor('sponsor');
  const seats = await match.methods.balanceOf(sponsor_address).call();

  assert.equal(units, seats);

});

getOrCreateBuyActor = async (name) => {
  // Get the account of the sponsor
  if (!state.actors[name]) {
    const accounts = await web3.eth.getAccounts();
    state.actors[name] = accounts[Object.keys(state.actors).length]
  }
  return state.actors[name];
}