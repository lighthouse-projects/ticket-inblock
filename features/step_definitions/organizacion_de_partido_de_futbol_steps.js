const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const featuresHelper = require('../FeaturesHelper')
let web3;
let accounts;
let state = {
  matchTokenContract: '',
  error: ''
}

//
// Escenario: Beltrán organiza el partido Mallorca-Albacete
//
Given('Partidator operativo', async function () {
  // Write code here that turns the phrase above into concrete actions
  //return 'pending';
  web3 = require('../../ethereum/web3-local');
  accounts = await web3.eth.getAccounts();
  matchToken = require('../../ethereum/build/MatchToken');
});

When('Beltrán organiza el {string}-{string} para el {string} en el estadio {string} con aforo de {int} localidades'
    , async function (local_team, visitor_team, match_date, stadium, capacity) {
  // Write code here that turns the phrase above into concrete actions
  await featuresHelper.deployMatchToken(accounts[0], local_team, visitor_team, match_date, stadium, capacity, (_error, _contract) => {state.error = _error; state.matchTokenContract = _contract});
  assert.ok(state.matchTokenContract.options.address, 'Smart contract for matches not deployed!');
});

Then('El equipo local del partido es el {string}', async function (_local_team) {
  // Write code here that turns the phrase above into concrete actions
  const local_team = await state.matchTokenContract.methods.local_team().call();
  assert.equal(local_team, _local_team);
});

Then('El equipo visitante del partido es el {string}', async function (_visitor_team) {
  // Write code here that turns the phrase above into concrete actions
  const visitor_team = await state.matchTokenContract.methods.visitor_team().call();
  assert.equal(visitor_team, _visitor_team);
});

Then('El partido se jugará el {string}', async function (_match_date) {
  // Write code here that turns the phrase above into concrete actions
  const moment = require('moment')(_match_date, featuresHelper.date_format);
  const dateUnix = moment.format('X');

  const match_date = await state.matchTokenContract.methods.match_date().call();
  assert.equal(match_date, dateUnix);

});

Then('El partido se jugará en el estadio {string}', async function (_stadium) {
  // Write code here that turns the phrase above into concrete actions
  const stadium = await state.matchTokenContract.methods.stadium().call();
  assert.equal(stadium, _stadium);
});

Then('El aforo es de {int} localidades', async function (_totalSupply) {
  // Write code here that turns the phrase above into concrete actions
  const totalSupply = await state.matchTokenContract.methods.totalSupply().call();
  assert.equal(totalSupply, _totalSupply);
});

Then('Beltrán es el propietario de todas las localidades del partido', async function () {
  const sponsor = await state.matchTokenContract.methods.sponsor().call();
  const expected = await state.matchTokenContract.methods.totalSupply().call();
  const result = await state.matchTokenContract.methods.balanceOf(sponsor).call();
  assert.equal(expected, result);
});

//
// Escenario: Beltrán organiza un partido del Mallorca pero no encuentra equipo rival
//
When('Beltrán organiza un partido del {string} para el {string} en el estadio {string} con aforo de {int} localidades', async function (local_team, match_date, stadium, capacity) {
  // Write code here that turns the phrase above into concrete actions
  await featuresHelper.deployMatchToken(accounts[0], local_team, '', match_date, stadium, capacity, (_error, _contract) => {state.error = _error; state.matchTokenContract = _contract});
});

Then('La organización del encuentro es rechazado por falta de rival', function () {
  // Write code here that turns the phrase above into concrete actions
  assert.ok(state.error, 'Smart contract deploy must fail if visitor team is empty');
  assert.equal(state.error.reason, 'Visitor team empty', 'When visitor team is empty the error must be "Visitor team empty"');
});

When('Beltrán organiza el {string}-{string} para el {string} sin estadio', async function (local_team, visitor_team, match_date) {
  // Write code here that turns the phrase above into concrete actions
  await featuresHelper.deployMatchToken(accounts[0], local_team, visitor_team, match_date, '', '', (_error, _contract) => {state.error = _error; state.matchTokenContract = _contract});
});
Then('La organización del encuentro es rechazado por falta de estadio', function () {
  // Write code here that turns the phrase above into concrete actions
  assert.ok(state.error, 'Smart contract deploy must fail if stadium is empty');
  assert.equal(state.error.reason, 'Stadium is empty', 'When stadium is empty the error must be "Stadium is empty"');
});

When('Beltrán organiza el {string}-{string} para el {string} en el estadio {string} pero desconoce el aforo', async function (local_team, visitor_team, match_date, stadium) {
  // Write code here that turns the phrase above into concrete actions
  await featuresHelper.deployMatchToken(accounts[0], local_team, visitor_team, match_date, stadium, '', (_error, _contract) => {state.error = _error; state.matchTokenContract = _contract});
});
Then('La organización del encuentro es rechazado por falta de aforo', function () {
  // Write code here that turns the phrase above into concrete actions
  assert.ok(state.error, 'Smart contract deploy must fail if capacity is empty');
  assert.equal(state.error.reason, 'Number of seats empty or 0', 'When capacity is empty the error must be "Number of seats empty or 0"');
});

When('Beltrán organiza el {string}-{string} con {int} días de retraso en el estadio {string} con aforo de {int} localidades', async function (local_team, visitor_team, delay, stadium, capacity) {
  const match_date = new Date();
  match_date.setDate(match_date.getDate() - delay);
  await featuresHelper.deployMatchToken(accounts[0], local_team, visitor_team, match_date, stadium, capacity, (_error, _contract) => {state.error = _error; state.matchTokenContract = _contract});
});

Then('La organización del encuentro es rechazado por fecha pasada', function () {
  // Write code here that turns the phrase above into concrete actions
  assert.ok(state.error, 'Match date in the past must fail');
  assert.equal(state.error.reason, 'Match created in the past', 'Match date in the past must fail with reason "Match created in the past"');
});