# language: es
Característica: Organización de partido de fútbol
  Organización de partido de fútbol

  Escenario: Beltrán organiza el partido Mallorca-Albacete
    Dado Partidator operativo
    Cuando Beltrán organiza el 'Mallorca'-'Albacete' para el '19/05/2019' en el estadio 'Mr Cat' con aforo de 23142 localidades
    Entonces El equipo local del partido es el 'Mallorca'
    Entonces El equipo visitante del partido es el 'Albacete'
    Entonces El partido se jugará el '19/05/2019'
    Entonces El partido se jugará en el estadio 'Mr Cat'
    Entonces El aforo es de 23142 localidades
    Entonces Beltrán es el propietario de todas las localidades del partido

  Escenario: Beltrán organiza un partido del Mallorca pero no encuentra equipo rival
    Dado Partidator operativo
    Cuando Beltrán organiza un partido del 'Mallorca' para el '19/05/2019' en el estadio 'Mr Cat' con aforo de 23142 localidades
    Entonces La organización del encuentro es rechazado por falta de rival

  Escenario: Beltrán organiza un partido pero no dispone de estadio
    Dado Partidator operativo
    Cuando Beltrán organiza el 'Mallorca'-'Albacete' para el '19/05/2019' sin estadio
    Entonces La organización del encuentro es rechazado por falta de estadio

  Escenario: Beltrán organiza un partido pero desconoce el aforo del estadio
    Dado Partidator operativo
    Cuando Beltrán organiza el 'Mallorca'-'Albacete' para el '19/05/2019' en el estadio 'Mr Cat' pero desconoce el aforo
    Entonces La organización del encuentro es rechazado por falta de aforo
    
  Escenario: Beltrán organiza un partido a fecha pasada
    Dado Partidator operativo
    Cuando Beltrán organiza el 'Mallorca'-'Albacete' con 2 días de retraso en el estadio 'Mr Cat' con aforo de 23142 localidades
    Entonces La organización del encuentro es rechazado por fecha pasada
