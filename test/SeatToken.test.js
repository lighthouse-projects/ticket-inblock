const web3 = require('../ethereum/web3-local');
const assert = require('assert');
const {interface, bytecode} = require('../ethereum/build/MatchToken');

let accounts;

beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
});

describe('SeatToken test', () => {
    it('Create SeatToken contract', async () => {
        const local_team = 'Mallorca';
        const visitor_team = 'Caceres';
        const date = 1546470000; // 2019-01-03
        const creator = accounts[0];

        const deploy = await new web3.eth.Contract(JSON.parse(interface))
                                                .deploy({
                                                    data: bytecode,
                                                    arguments: [local_team, visitor_team, date]
                                                });
        
        const _gas = await deploy.estimateGas();
        console.log('Estimated gas', _gas);
        const contract = await deploy.send({
            from: creator,
            gas: _gas
        });

        assert.ok(contract.options.address, 'The contract is not been created');
        assert.equal(local_team, await contract.methods.local_team().call());
        assert.equal(visitor_team, await contract.methods.visitor_team().call());
        assert.equal(date, await contract.methods.date().call());
        assert.equal(creator, await contract.methods.sponsor().call());
    });
})