var gulp = require('gulp');
var compile = require('./ethereum/compileFunction');
var cucumber = require('gulp-cucumber');

function cucumberFeatures() {
    return gulp.src('features/*').pipe(cucumber({
    'steps': 'features/step_definitions/*.js',
    'format': 'summary'
  }));
}

gulp.task('default', function() {
  cucumberFeatures();
});

gulp.task('compile', function() {
    // Compile all smart contracts
    compile();
});

gulp.task('cucumber', function() {
  return cucumberFeatures();
});

// Watch contracts and compile when changed
gulp.watch('ethereum/contracts/**/*.sol').on('change', function(event) {
  compileContract(event.path);
  return cucumberFeatures();
});
gulp.watch('ethereum/contracts/**/*.sol').on('add', function(event) {
  compileContract(event.path);
  return cucumberFeatures();
});

// Watch features and launch cucumber when changed
gulp.watch(['features/**'], ['cucumber']);